class CellState:
    DEAD = 0
    ALIVE = 1


class GameOfLife:
    def __init__(self, starting_grid):
        self.grid_state = starting_grid

        self.grid_height = len(starting_grid)

        # TODO: Need a check here to make sure we have a grid height > 0
        self.grid_width = len(starting_grid[0])

    def move_to_and_get_next_generation(self):
        #  Initialize an empty next grid state
        next_grid_state = list()

        for row_index, row in enumerate(self.grid_state):
            new_row = list()
            for column_index, cell_state in enumerate(row):
                num_live_neighbors = self._get_cells_live_neighbors(row_index, column_index)
                next_cell_state = self._get_cell_state_for_next_generation(cell_state, num_live_neighbors)
                new_row.append(next_cell_state)

            next_grid_state.append(new_row)

        self.grid_state = next_grid_state

        return self.grid_state

    def _get_cell_state_for_next_generation(self, current_cell_state, neighbor_count):

        if current_cell_state == CellState.ALIVE:
            if neighbor_count < 2:
                return CellState.DEAD

            if neighbor_count > 3:
                return CellState.DEAD

        else:  # Dead Cell State
            if neighbor_count == 3:
                return CellState.ALIVE

        # If none of the conditions are met then the state stays the same
        return current_cell_state

    def _get_cells_live_neighbors(self, row_index, column_index):
        neighbor_count = 0

        # TODO: Is there a more elegant way to do this than just checking indexes one by one?
        # TODO: There has to be because this is gross

        # Check cells to the left
        if row_index >= 1:
            if self.grid_state[row_index-1][column_index] == 1:
                neighbor_count += 1

            # Check cell to top left
            if column_index >= 1:
                if self.grid_state[row_index-1][column_index - 1] == 1:
                    neighbor_count += 1

            # Check cell to bottom left
            if column_index < self.grid_height-1:
                if self.grid_state[row_index-1][column_index + 1] == 1:
                    neighbor_count += 1

        # Check cells to the right
        if row_index < self.grid_width-1:
            if self.grid_state[row_index + 1][column_index] == 1:
                neighbor_count += 1

            # Check cell to top right
            if column_index >= 1:
                if self.grid_state[row_index + 1][column_index - 1] == 1:
                    neighbor_count += 1

            # Check cell to bottom right
            if column_index < self.grid_height-1:
                if self.grid_state[row_index + 1][column_index + 1] == 1:
                    neighbor_count += 1

        # Check cell to bottom
        if column_index < self.grid_height-1:
            if self.grid_state[row_index][column_index + 1] == 1:
                neighbor_count += 1

        # Check cell to top
        if column_index >= 1:
            if self.grid_state[row_index][column_index - 1] == 1:
                neighbor_count += 1

        return neighbor_count