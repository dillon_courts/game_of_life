import unittest
from game_of_life import GameOfLife, CellState


class GameOfLifeTest(unittest.TestCase):

    def setUp(self):
        pass

    def test_live_cell_with_fewer_than_two_live_neighbors_dies(self):
        game_of_life = GameOfLife([
            [0, 0, 0],
            [1, 0, 0],
            [0, 0, 0]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.DEAD

        game_of_life = GameOfLife([
            [0, 0, 0],
            [1, 0, 0],
            [1, 0, 0]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.DEAD

    def test_live_cell_with_more_than_three_live_neighbors_dies(self):
        game_of_life = GameOfLife([
            [1, 1, 1],
            [1, 1, 1],
            [1, 1, 1]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.DEAD

    def test_live_cell_with_two_or_three_live_neighbors_lives(self):
        game_of_life = GameOfLife([
            [0, 0, 0],
            [1, 1, 0],
            [0, 1, 0]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.ALIVE

        game_of_life = GameOfLife([
            [0, 0, 0],
            [1, 1, 0],
            [1, 1, 0]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.ALIVE

    def test_dead_cell_with_exactly_three_live_neighbors_becomes_live(self):
        game_of_life = GameOfLife([
            [0, 1, 0],
            [1, 0, 0],
            [0, 1, 0]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][1] == CellState.ALIVE
